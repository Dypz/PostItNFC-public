﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.ServiceModel.Web;
using System.Text;

namespace DatabaseCRUDRESTService.Authorisations
{
    [ServiceContract]
    public interface IAuthorisationService
    {
        [OperationContract]
        [WebInvoke(Method = "GET", UriTemplate = "GetAuthorisations?id={id}",
            ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        Authorisation GetUserAuthorisations(string id);

        [OperationContract]
        [WebInvoke(Method = "GET", UriTemplate = "GetGuestAuthorisations?mail={mail}",
            ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        Authorisation GetGuestAuthorisations(string mail);

        [OperationContract]
        [WebInvoke(Method = "POST", UriTemplate = "SetAuthorisations/",
                    RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Bare)]
        bool SetAuthorisations(Authorisation authorisation);

        [OperationContract]
        [WebInvoke(Method = "POST", UriTemplate = "UpdateAuthorisations/",
                    RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Bare)]
        bool UpdateAuthorisations(Authorisation authorisation);
    }
}
