﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Web;

namespace DatabaseCRUDRESTService.Authorisations
{
    [DataContract]
    public class Authorisation
    {
        [DataMember(Name = "FirstNameAut")]
        public bool FirstNameAut { get; set; }

        [DataMember(Name = "LastNameAut")]
        public bool LastNameAut { get; set; }

        [DataMember(Name = "MailAut")]
        public bool MailAut { get; set; }

        [DataMember(Name = "RoomAut")]
        public bool RoomAut { get; set; }

        [DataMember(Name = "RoleAut")]
        public bool RoleAut { get; set; }

        [DataMember(Name = "UserID")]
        public string UserID { get; set; }

        [DataMember(Name = "UserMail")]
        public string UserMail { get; set; }
    }
}