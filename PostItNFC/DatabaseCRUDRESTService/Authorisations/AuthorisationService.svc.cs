﻿using MySql.Data.MySqlClient;
using System;
using System.Configuration;
using System.Data.SqlClient;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;

namespace DatabaseCRUDRESTService.Authorisations
{
    public class AuthorisationService : IAuthorisationService
    {
        private string STR_CONNECTION = ConfigurationManager.ConnectionStrings["connString"].ConnectionString;
        public Authorisation GetUserAuthorisations(string id)
        {
            Authorisation authorisation = null;
            using (SqlConnection con = new SqlConnection(STR_CONNECTION))
            {
                con.Open();
                string cmdText = "SELECT first_name,last_name,mail,room_id,role,user_mail FROM dbo.guest_authorisations WHERE user_id='" + id + "';";
                using (SqlCommand cmd = new SqlCommand(cmdText))
                {
                    cmd.Connection = con;
                    SqlDataReader reader = cmd.ExecuteReader();
                    if (reader.Read())
                    {
                        authorisation = new Authorisation()
                        {
                            FirstNameAut = Convert.ToBoolean(reader["first_name"]),
                            LastNameAut = Convert.ToBoolean(reader["last_name"]),
                            MailAut = Convert.ToBoolean(reader["mail"]),
                            RoomAut = Convert.ToBoolean(reader["room_id"]),
                            RoleAut = Convert.ToBoolean(reader["role"]),
                            UserMail = reader["user_mail"].ToString(),
                            UserID = id
                        };

                    }
                }
            }
            return authorisation;
        }


        public Authorisation GetUserAuthorisations()
        {
            Authorisation authorisation = null;
            using (SqlConnection con = new SqlConnection(STR_CONNECTION))
            {
                con.Open();
                string cmdText =
                    "SELECT first_name,last_name,mail,room_id,role,user_mail FROM dbo.guest_authorisations;";
                using (SqlCommand cmd = new SqlCommand(cmdText))
                {
                    cmd.Connection = con;
                    SqlDataReader reader = cmd.ExecuteReader();
                }
            }
            return authorisation;
        }

        public Authorisation GetGuestAuthorisations(string mail)
        {
            Authorisation authorisation = null;
            using (SqlConnection con = new SqlConnection(STR_CONNECTION))
            {
                con.Open();
                string cmdText = "SELECT first_name,last_name,mail,room_id,role,user_id FROM guest_authorisations WHERE user_mail='" + mail + "';";
                using (SqlCommand cmd = new SqlCommand(cmdText))
                {
                    cmd.Connection = con;
                    SqlDataReader reader = cmd.ExecuteReader();
                    if (reader.Read())
                    {
                        authorisation = new Authorisation()
                        {
                            FirstNameAut = Convert.ToBoolean(reader["first_name"]),
                            LastNameAut = Convert.ToBoolean(reader["last_name"]),
                            MailAut = Convert.ToBoolean(reader["mail"]),
                            RoomAut = Convert.ToBoolean(reader["room_id"]),
                            RoleAut = Convert.ToBoolean(reader["role"]),
                            UserID = reader["user_id"].ToString(),
                            UserMail = mail
                        };

                    }
                }
            }
            return authorisation;
        }

        public bool SetAuthorisations(Authorisation authorisation)
        {
            using (SqlConnection con = new SqlConnection(STR_CONNECTION))
            {
                con.Open();
                SqlCommand command = con.CreateCommand();
                command.CommandText = "INSERT INTO guest_authorisations (first_name,last_name,mail,room_id,role,user_id,user_mail) VALUES ('" + Convert.ToInt32(authorisation.FirstNameAut) +
                                    "','" + Convert.ToInt32(authorisation.LastNameAut) + "','" + Convert.ToInt32(authorisation.MailAut) + "','" + Convert.ToInt32(authorisation.RoomAut) +
                                    "','" + Convert.ToInt32(authorisation.RoleAut) + "','" + authorisation.UserID + "','" + authorisation.UserMail + "');";
                command.Connection = con;
                command.ExecuteNonQuery();
                return true;
            }
        }



        public bool UpdateAuthorisations(Authorisation authorisation)
        {

            using (SqlConnection con = new SqlConnection(STR_CONNECTION))
            {

                con.Open();
                SqlCommand command = con.CreateCommand();
                command.CommandText =
                    "UPDATE guest_authorisations SET first_name='"+Convert.ToInt32(authorisation.FirstNameAut)+ "', last_name = '" + Convert.ToInt32(authorisation.LastNameAut) + "',mail = '" + Convert.ToInt32(authorisation.MailAut) + "', room_id = '" + Convert.ToInt32(authorisation.RoomAut) + "', role = '" + Convert.ToInt32(authorisation.RoleAut) + "' WHERE user_id ='"+authorisation.UserID+ "'";
                
                command.Connection = con;
                command.ExecuteNonQuery();
                return true;
            }
        }
    }
}
