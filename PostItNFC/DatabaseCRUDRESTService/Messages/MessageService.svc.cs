﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;

namespace DatabaseCRUDRESTService.Messages
{
    public class MessageService : IMessageService
    {
        private string STR_CONNECTION = ConfigurationManager.ConnectionStrings["connString"].ConnectionString;
        public List<Message> GetMessagesRoomList(string id, string roomID)
        {
            List<Message> result = new List<Message>();
            using (SqlConnection con = new SqlConnection(STR_CONNECTION))
            {
                // get only available messages and the message not created by the user who is requested the list of messages
                string query = @"SELECT * FROM messages WHERE expiration_date > GETDATE() AND author_id !='" + id + "' AND associated_room_id='" + roomID + "' ";
                using (SqlCommand cmd = new SqlCommand(query))
                {
                    using (SqlDataAdapter sda = new SqlDataAdapter())
                    {
                        cmd.Connection = con;
                        sda.SelectCommand = cmd;
                        using (DataTable dt = new DataTable())
                        {
                            sda.Fill(dt);
                            foreach (DataRow row in dt.Rows)
                            {
                                result.Add(new Message()
                                {
                                    Content = row["content"].ToString(),
                                    MessageID = row["id"].ToString(),
                                    Security = (SecurityLevel)row["security_level"],
                                    AuthorID = row["author_id"].ToString(),
                                    AuthorFirstName = row["author_first_name"].ToString(),
                                    AuthorLastName = row["author_last_name"].ToString(),
                                    CreationDate = (DateTime)row["creation_date"],
                                    ExpirationDate = (DateTime)row["expiration_date"],
                                    ReceiversList = row["receivers"].ToString(),
                                    AssociatedRoomID = row["associated_room_id"].ToString()
                                });
                            }

                        }
                    }
                }
            }
            return result;
        }

        public List<Message> GetCreatedMessages(string id)
        {
            List<Message> result = new List<Message>();
            using (SqlConnection con = new SqlConnection(STR_CONNECTION))
            {
                string query = "SELECT * FROM messages WHERE author_id='" +id +"';";
                using (SqlCommand cmd = new SqlCommand(query))
                {
                    using (SqlDataAdapter sda = new SqlDataAdapter())
                    {
                        cmd.Connection = con;
                        sda.SelectCommand = cmd;
                        using (DataTable dt = new DataTable())
                        {
                            sda.Fill(dt);
                            foreach (DataRow row in dt.Rows)
                            {
                                result.Add(new Message()
                                {
                                    Content = row["content"].ToString(),
                                    MessageID = row["id"].ToString(),
                                    Security = (SecurityLevel)row["security_level"],
                                    AuthorID = row["author_id"].ToString(),
                                    AuthorFirstName = row["author_first_name"].ToString(),
                                    AuthorLastName = row["author_last_name"].ToString(),
                                    CreationDate = (DateTime)row["creation_date"],
                                    ExpirationDate = (DateTime)row["expiration_date"],
                                    ReceiversList = row["receivers"].ToString(),
                                    AssociatedRoomID = row["associated_room_id"].ToString()
                                });
                            }

                        }
                    }
                }
            }
            return result;
        }

        public bool CreateMessage(Message message)
        {
            using (SqlConnection con = new SqlConnection(STR_CONNECTION))
            {
                con.Open();
                SqlCommand command = con.CreateCommand();
                command.CommandText = "INSERT INTO messages (id,content,security_level,author_id,author_first_name,author_last_name,creation_date,expiration_date,receivers,associated_room_id) VALUES ('" + message.MessageID +
                    "','" + message.Content + "','" + (int)message.Security + "','" + message.AuthorID + "','" + message.AuthorFirstName +
                    "','" + message.AuthorLastName + "','" + message.CreationDate.ToString("yyyy-MM-dd H:mm:ss") + "','" + message.ExpirationDate.ToString("yyyy-MM-dd H:mm:ss") +
                    "','" + message.ReceiversList + "','" + message.AssociatedRoomID + "');";
                command.Connection = con;
                command.ExecuteNonQuery();
                
                return true;
            }
        }

        public bool UpdateMessage(Message message)
        {
            using (SqlConnection con = new SqlConnection(STR_CONNECTION))
            {

                con.Open();
                SqlCommand command = con.CreateCommand();
                command.CommandText = "UPDATE messages SET content='" + message.Content + "', security_level='" + (int)message.Security +
                                        "', author_id='" + message.AuthorID + "', author_first_name='" + message.AuthorFirstName +
                                        "', author_last_name='" + message.AuthorLastName + "', creation_date='" + message.CreationDate.ToString("yyyy-MM-dd H:mm:ss") +
                                        "', expiration_date='" + message.ExpirationDate.ToString("yyyy-MM-dd H:mm:ss") +
                                        "', receivers='" + message.ReceiversList + "', associated_room_id='" + message.AssociatedRoomID +
                                        "' WHERE id='" + message.MessageID + "';";
                command.Connection = con;
                command.ExecuteNonQuery();
                return true;
            }
        }

        public bool RemoveMessage(string messageId)
        {
            using (SqlConnection con = new SqlConnection(STR_CONNECTION))
            {
                con.Open();
                SqlCommand command = con.CreateCommand();
                command.CommandText = "DELETE FROM messages WHERE id='" + messageId + "';";
                command.Connection = con;
                command.ExecuteNonQuery();
                return true;
            }
        }
    }
}
