﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Web;

namespace DatabaseCRUDRESTService.Messages
{
    [DataContract]
    public class Message
    {
        #region Messages fields web service
        [DataMember(Name = "Content")]
        public string Content { get; set; }

        [DataMember(Name = "Security")]
        public SecurityLevel Security { get; set; }

        [DataMember(Name = "AuthorID")]
        public string AuthorID { get; set; }

        [DataMember(Name = "MessageID")]
        public string MessageID { get; set; }

        [DataMember(Name = "AuthorFirstName")]
        public string AuthorFirstName { get; set; }

        [DataMember(Name = "AuthorLastName")]
        public string AuthorLastName { get; set; }

        [DataMember(Name = "CreationDate")]
        public DateTime CreationDate { get; set; }

        [DataMember(Name = "ExpirationDate")]
        public DateTime ExpirationDate { get; set; }

        [DataMember(Name = "ReceiversList")]
        public string ReceiversList { get; set; } = null;

        [DataMember(Name = "AssociatedRoomID")]
        public string AssociatedRoomID { get; set; }
        #endregion
    }

    public enum SecurityLevel { Public = 0, AuthenticatedUsers = 1, ReceiversDefine = 2 }
}