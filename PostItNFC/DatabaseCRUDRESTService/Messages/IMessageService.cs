﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.ServiceModel.Web;
using System.Text;

namespace DatabaseCRUDRESTService.Messages
{
    [ServiceContract]
    public interface IMessageService
    {
        [OperationContract]
        [WebInvoke(Method = "GET", UriTemplate = "GetMessagesRoom/id={id}/roomID={roomID}",
             ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        List<Message> GetMessagesRoomList(string id, string roomID);

        [OperationContract]
        [WebInvoke(Method = "GET", UriTemplate = "GetCreatedMessages/id={id}",
             ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        List<Message> GetCreatedMessages(string id);

        [OperationContract]
        [WebInvoke(Method = "POST", UriTemplate = "CreateMessage/",
                    RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Bare)]
        bool CreateMessage(Message message);

        [OperationContract]
        [WebInvoke(Method = "POST", UriTemplate = "UpdateMessage/",
                    RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Bare)]
        bool UpdateMessage(Message message);

        [OperationContract]
        [WebInvoke(Method = "GET", UriTemplate = "RemoveMessage/id={id}",
                    RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Bare)]
        bool RemoveMessage(string id);
    }
}
