﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Text;
using System.Threading.Tasks;
using Microsoft.IdentityModel.Clients.ActiveDirectory;

namespace DirectoryRetrieverRESTService
{
    // REMARQUE : vous pouvez utiliser la commande Renommer du menu Refactoriser pour changer le nom de classe "DirectoryRetrieverService" à la fois dans le code, le fichier svc et le fichier de configuration.
    // REMARQUE : pour lancer le client test WCF afin de tester ce service, sélectionnez DirectoryRetrieverService.svc ou DirectoryRetrieverService.svc.cs dans l'Explorateur de solutions et démarrez le débogage.
    [AspNetCompatibilityRequirements(RequirementsMode = AspNetCompatibilityRequirementsMode.Allowed)]

    public class DirectoryRetrieverService : IDirectoryRetrieverService
    {

        private string userToken = "";
        private const string graphUrl = "https://graph.microsoft.com";
        private const string version = "v1.0";
        private const string organisation = "organization";



        public async Task<string> GetUsers()
        {
            if (userToken == "")
            {
                userToken = await GetTokenForUserAsync();

            }
            try
            {
                using (var client = new HttpClient())
                {
                    client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", userToken);
                    client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                    var url = $"{graphUrl}/{version}/myorganization/users?api-version[v1.0]";

                    var resultAsString = await client.GetStringAsync(url);


                    return resultAsString;
                }
            }
            catch (Exception ex)
            {
                var result = ex.Message;
                return result;
            }
        }

        private static async Task<string> GetTokenForUserAsync()
        {
            var authority = string.Format("https://login.microsoftonline.com/{0}", "arnaudannuaire.onmicrosoft.com");
            ClientCredential clientCredential = new ClientCredential("36e4f5a9-9758-4190-9e59-108a349042f8", "geinZ97Ee3y2zklV4zYbfyOp/BHoEt365HHI/3RPtaQ=");
            AuthenticationContext authenticationContext = new AuthenticationContext(authority);
            AuthenticationResult authResult;
            try
            {
                authResult = await authenticationContext.AcquireTokenAsync("https://graph.microsoft.com", clientCredential);
                return authResult.AccessToken;
            }
            catch (Exception ex)
            {

                return ex.Message;
            }
        }
    }
}
