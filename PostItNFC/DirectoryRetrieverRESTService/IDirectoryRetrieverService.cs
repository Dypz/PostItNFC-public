﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.ServiceModel.Web;
using System.Text;
using System.Threading.Tasks;

namespace DirectoryRetrieverRESTService
{
    // REMARQUE : vous pouvez utiliser la commande Renommer du menu Refactoriser pour changer le nom d'interface "IDirectoryRetrieverService" à la fois dans le code et le fichier de configuration.
    [ServiceContract]

    public interface IDirectoryRetrieverService
    {

        [WebInvoke(Method = "GET", UriTemplate = "User/",
                ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
            Task<string> GetUsers();

        }
    }

