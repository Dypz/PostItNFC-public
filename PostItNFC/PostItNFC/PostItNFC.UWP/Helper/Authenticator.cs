﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.IdentityModel.Clients.ActiveDirectory;
using PostItNFC.Helper;
using Xamarin.Forms;

[assembly: Dependency(typeof(PostItNFC.UWP.Helper.Authenticator))]
namespace PostItNFC.UWP.Helper
{
    class Authenticator : IAuthenticator
    {
        AuthenticationContext authContext ;

        public async Task<AuthenticationResult> Authenticate(string authority, string resource, string clientId, string returnUri, bool onSignIn)
        {
            authContext = new AuthenticationContext(authority);
            if (authContext.TokenCache.ReadItems().Any())
                authContext = new AuthenticationContext(authContext.TokenCache.ReadItems().First().Authority);
            else if(!onSignIn)
            {
                return null;
            }
            var authResult =
                await
                    authContext.AcquireTokenAsync(resource, clientId, new Uri(returnUri),
                        new PlatformParameters(PromptBehavior.Auto, false));
            return authResult;
        }

        public void LogOut()
        {
            authContext.TokenCache.Clear();    
        }
    }
}
