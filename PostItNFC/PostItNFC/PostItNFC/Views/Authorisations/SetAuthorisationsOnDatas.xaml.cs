﻿using System;
using System.Linq;
using PostItNFC.Model;
using PostItNFC.Views.ManagerUsersRoom;
using PostItNFC.Services;

using Xamarin.Forms;

namespace PostItNFC.Views.Authorisations
{
    public partial class SetAuthorisationsOnDatas : ContentPage
    {
        #region SetAuthorisationsOnDatas Fields
        private User CurrentUser { get; set; } = null;
        #endregion

        #region SetAuthorisationsOnDatas Constructor
        public SetAuthorisationsOnDatas(User user)
        {
            InitializeComponent();
            CurrentUser = user;
            confirmButton.Clicked += (s, e) => onConfirm(s, e);
        }
        #endregion

        #region SetAuthorisationsOnDatas Events methods
        public void onConfirm(object sender, EventArgs e)
        {
            FieldsUser firstNameAut = guestAuthorisations.FieldsName.First(s => s.DatabaseName.Equals("FirstName"));
            FieldsUser lastNameAut = guestAuthorisations.FieldsName.First(s => s.DatabaseName.Equals("LastName"));
            FieldsUser mailAut = guestAuthorisations.FieldsName.First(s => s.DatabaseName.Equals("Mail"));
            FieldsUser roomAut = guestAuthorisations.FieldsName.First(s => s.DatabaseName.Equals("IdRoom"));
            FieldsUser roleAut = guestAuthorisations.FieldsName.First(s => s.DatabaseName.Equals("Role"));

            Authorisation authorisation = new Authorisation(firstNameAut.Enabled, lastNameAut.Enabled, mailAut.Enabled,
                                                            roomAut.Enabled, roleAut.Enabled, CurrentUser.Id, CurrentUser.Mail);

            if(RESTAuthorisationService.GetAuthorisation(CurrentUser.Id) == null)
            {
                CreateAuthorisations(authorisation);
            }
            else
            {
                UpdateAuthorisations(authorisation);
            }
            var mainPage = GlobalConstants.GetMainPage();
            mainPage.Detail = new NavigationPage(new UsersFindForRoom(CurrentUser));
        }
        #endregion

        #region SetAuthorisationsOnDatas methods
        private void UpdateAuthorisations(Authorisation authorisation)
        {
            if (RESTAuthorisationService.UpdateAuthorisation(authorisation))
            {
                DisplayAlert("Success ! ", "Authorisations successfully updated", "OK");

            }
            else
            {
                DisplayAlert("Alert ! ", "A problem occured when updating the authorisations, try again", "OK");
            }
        }

        private void CreateAuthorisations(Authorisation authorisation)
        {
            if (RESTAuthorisationService.SetAuthorisations(authorisation))
            {
                DisplayAlert("Success ! ", "Authorisations successfully set", "OK");
            }
            else
            {
                DisplayAlert("Alert ! ", "A problem occured when setting the authorisations, try again", "OK");
            }
        }

        #endregion

    }
}
