﻿using System.Collections.Generic;
using System.Text;
using PostItNFC.Model;
using System.Reflection;

using Xamarin.Forms;

namespace PostItNFC.Views.Authorisations
{
    public partial class ListAuthorisations : ContentView
    {
        #region ListAuthorisations Fields
        public List<FieldsUser> FieldsName { get; set; }

        public string Prompt { get { return prompt.Text; } set { prompt.Text = value; } }

        #endregion

        #region ListAuthorisations
        public ListAuthorisations()
        {
            InitializeComponent();
            FieldsName = new List<FieldsUser>();
            foreach(KeyValuePair<string,bool> entry in Services.RESTAuthorisationService.GetGuestAuthorisation(App.UserObtained.Mail).ExploreAuth())
            {
                FieldsName.Add(new FieldsUser(entry.Key, entry.Value));
            }
            DefineTemplatesOfList(listAuthorisations);
        }
        #endregion

        #region ListAuthorisations Initialisation Methods
        public void DefineTemplatesOfList(ListView listView)
        {
            listView.ItemsSource = FieldsName;
            listView.ItemTemplate = new DataTemplate(typeof(SwitchCell));
            listView.ItemTemplate.SetBinding(SwitchCell.TextProperty, "FieldName");
            listView.ItemTemplate.SetBinding(SwitchCell.OnProperty, "Enabled");
        }
        #endregion
    }
}
