﻿using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Threading.Tasks;
using PostItNFC.Model;
using Xamarin.Forms;
using PostItNFC.Views.Messages;
using PostItNFC.Views.Authorisations;
using PostItNFC.Services;
using PostItNFC.Helper;

namespace PostItNFC.Views.ManagerUsersRoom
{
    public partial class UsersFindForRoom : ContentPage
    {
        #region UsersFindForRoom Attributes
        private User CurrentUser { get; set; } = new User();

        private List<User> UsersFind { get; set; } = null;

        private Dictionary<string,Authorisation> Authorisations { get; set; }
        private User SelectedtItem { get; set; } = null;

        private List<Message> MessagesFind { get; set; } = new List<Message>();

        private string RoomTagged { get; set; } = null;

        private List<Message> CreatedMessages { get; set; }
        #endregion

        #region UsersFindForRoom Constructors

        // constructor for an authenticated user
        public UsersFindForRoom(User currentUser)
        {
            InitializeComponent();

            CurrentUser = currentUser;

            CommonConstructorMethods();
        }

        // this constructor does not need roomTagged because it's a guest so the user can't let a message
        public UsersFindForRoom(UsersRole.Role role)
        {
            InitializeComponent();

            CurrentUser.Role = UsersRole.Role.Guest;

            CommonConstructorMethods();
        }

        public void CommonConstructorMethods()
        {
            RoomTagged = GlobalConstants.RoomTagged;
            // set title of the application
            this.Title = GlobalConstants.RoomTagged;

            // Display correct message depending on the number of users find
            GetUsersRoom();

            // Display a button to indicate if messages had been found for the current user
            GetMessages();

            // define the data template for the list view
            DataTemplateForListView();

            usersFindForRoom.ItemTapped += (s, e) => OnItemTapped(s, e);
        }
        #endregion

        #region UsersFindForRoom Methods events

        private void OnItemTapped(object sender, ItemTappedEventArgs e)
        {
            if (e.Item == null) return;
            var userSelected = (User)e.Item;

            Navigation.PushAsync(new NavigationPage(new UserSelected(userSelected, CurrentUser.Role)));
            SelectedtItem = userSelected;
            usersFindForRoom.SelectedItem = null;
        }

        private void onShowMessages(object sender, EventArgs e)
        {
            if (MessagesFind.Count == 0)
            {
                DisplayAlert("Alert ! ", "An error occured, try again ! ", "OK");
                return;
            }
            Navigation.PushAsync(new MessagesForUser(MessagesFind));
        }

        #endregion

        #region Methods for setting the UsersFindForRoom View

        private void DataTemplateForListView()
        {
            usersFindForRoom.ItemsSource = UsersFind;
            usersFindForRoom.ItemTemplate = new DataTemplate(() =>
            {
                // Create views with bindings for displaying each property.
                Label firstNameLabel = new Label();
                firstNameLabel.SetBinding(Label.TextProperty, "FirstName");
                firstNameLabel.FontAttributes = FontAttributes.Bold;


                Label lastNameLabel = new Label();
                lastNameLabel.SetBinding(Label.TextProperty, "LastName");
                lastNameLabel.FontAttributes = FontAttributes.Bold;

                Label roleLabel = new Label();
                roleLabel.SetBinding(Label.TextProperty, "Role");
                roleLabel.FontSize = 12;

                Label mailLabel = new Label();
                mailLabel.SetBinding(Label.TextProperty, "Mail");
                mailLabel.FontSize = 12;

                TextCell idLabel = new TextCell();
                idLabel.SetBinding(TextCell.TextProperty, "Id");

                // display first only first name and last name of the users find
                StackLayout stackLayout = new StackLayout
                {
                    Padding = new Thickness(10, 8),
                    Orientation = StackOrientation.Vertical,
                    Children =
                                {new StackLayout
                                    {
                                    Padding = new Thickness(0, 2),
                                    Orientation = StackOrientation.Horizontal,
                                    Children =
                                             {
                                                firstNameLabel,
                                                lastNameLabel
                                            }
                                    }
                                }
                };
                // a guest cannot immediately seen few details about the users
                // he had to click on it to see the informations according to the authorisations
                if(CurrentUser.Role != UsersRole.Role.Guest)
                    stackLayout.Children.Add(new StackLayout
                    {
                        Padding = new Thickness(23, 0),
                        Orientation = StackOrientation.Horizontal,
                        Children =
                                             {
                                                roleLabel,
                                                mailLabel
                                            }
                    });

                return new ViewCell
                {
                    View = stackLayout
                };
            });
        }


        private async Task GetUsersRoom()
        {
            if(GlobalConstants.RoomTagged == null)
            {
                usersFind.Text = "Aucune salle détectée, veuillez tag une salle";
                this.Title = "Pas de salle";
                sadness.IsVisible = false;
                return;
            }

            try
            {
                using (var client = new HttpClient())
                {
;
                    UsersFind = MicrosoftAPIs.GetUserOfRoom(GlobalConstants.RoomTagged);

                    if (UsersFind == null || UsersFind.Count == 0)
                    {
                        sadness.IsVisible = true;
                        usersFind.Text = "Ce bureau n'appartient à personne !!";
                        usersFindForRoom.IsVisible = false;
                    }
                    else
                    {
                        sadness.IsVisible = false;
                        usersFind.Text = "Vous êtes devant le bureau de : ";
                    }
                }

            }
            catch (Exception ex)
            {
                System.Diagnostics.Debug.WriteLine(ex.Message);
            }
        }

        private List<Message> RequestMessagesWebService()
        {
            return (CurrentUser.Id == null || CurrentUser.Id.Equals("")) ? RESTMessageService.GetAvailableMessagesForRoom("null",GlobalConstants.RoomTagged) : RESTMessageService.GetAvailableMessagesForRoom(CurrentUser.Id,GlobalConstants.RoomTagged);
        }

        private void GetMessages()
        {
            List<Message> allMessages = RequestMessagesWebService();
            // if none messages obtained
            if (allMessages == null)
            {
                messagesFind.IsVisible = false;
                return;
            }

            // we only received the available messages
            foreach(Message message in allMessages){
                // the messages is public : everybody can get it
                if (message.Security == SecurityLevel.Public)
                {
                    MessagesFind.Add(message);
                    continue;
                }

                // the message is private : only identified users can get it
                if (message.Security == SecurityLevel.AuthenticatedUsers && CurrentUser.Role != UsersRole.Role.Guest)
                {
                    MessagesFind.Add(message);
                    continue;
                }

                if(message.Security == SecurityLevel.ReceiversDefine && CurrentUser.Role != UsersRole.Role.Guest)
                {
                    List<string> mails = new List<string>(message.ReceiversList.Split(';'));
                    if(mails.Find(mail => mail.Equals(CurrentUser.Mail)) != null)
                    {
                        MessagesFind.Add(message);
                        continue;
                    }
                }
            }

            if (MessagesFind.Count > 0)
            {
                messagesFind.IsVisible = true;
                messagesFind.Text = MessagesFind.Count + " messages find for this room (Click to see them)";
                messagesFind.Clicked += (s, e) => onShowMessages(s, e);
            }
            else
            {
                messagesFind.IsVisible = false;
            }
        }
        #endregion
    }
}