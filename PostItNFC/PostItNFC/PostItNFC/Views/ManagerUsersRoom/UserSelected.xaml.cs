﻿using PostItNFC.Model;

using Xamarin.Forms;

namespace PostItNFC.Views.ManagerUsersRoom
{
    public partial class UserSelected : ContentPage
    {
        #region UserSelected constructors
        public UserSelected(User userSelected, UsersRole.Role currentRole)
        {
            InitializeComponent();

            // define values obtained for the selected user
            SetUserValues(userSelected);

            // display values according to the role of the current user
            if (currentRole == UsersRole.Role.Guest)
            {
                // if it's a guest user, display informations according to the authorisations
                SetElementsVisibilityForGuest(userSelected);
            }
            // otherwise a connected users can see all the informations about a user in a room
            
        }
        #endregion

        #region UserSelected methods
        private void SetElementsVisibilityForGuest(User userSelected)
        {
            Authorisation authorisation = Services.RESTAuthorisationService.GetGuestAuthorisation(userSelected.Mail);
            // the selected user has already defined his authorisations for a guest user
            if (authorisation != null)
            {
                roomId.IsVisible = authorisation.RoomAut;
                firstName.IsVisible = authorisation.FirstNameAut;
                lastName.IsVisible = authorisation.LastNameAut;
                email.IsVisible = authorisation.MailAut;
                role.IsVisible = authorisation.RoleAut;
            }
            else
            {
                // if the selected user has not defined his authorisations for a guest user
                // we choose to only display first name, last name and room
                email.IsVisible = false;
                role.IsVisible = false;
            }
        }
        private void SetUserValues(User userSelected)
        {
            roomId.Text = "Office : " + userSelected.IdRoom;
            firstName.Text = "First name : " +userSelected.FirstName;
            lastName.Text = "Last name : " + userSelected.LastName;
            email.Text = "Email : " + userSelected.Mail;
            role.Text = "Role : " + userSelected.Role.ToString();
        }
        #endregion
    }
}
