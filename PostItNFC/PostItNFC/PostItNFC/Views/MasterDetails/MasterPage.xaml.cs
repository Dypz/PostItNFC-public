﻿using System;
using System.Collections.Generic;
using PostItNFC.Model;

using Xamarin.Forms;

namespace PostItNFC.Views.MasterDetails
{
    public partial class MasterPage : ContentPage
    {
        #region MasterPage Fields
        public ListView ListView { get { return listView; } }
        #endregion

        #region MasterPage Constructor
        public MasterPage(UsersRole.Role role, string displayName)
        {
            InitializeComponent();
            var masterPageItems = new List<MasterPageItem>();
            masterPageItems.Add(new MasterPageItem
            {
                Title = displayName,
                ItemType = ItemMaster.DisplayName
            });

            if (role == UsersRole.Role.Guest)
            {
                masterPageItems.Add(new MasterPageItem
                {
                    Title = "Se connecter",
                    ItemType = ItemMaster.Login
                });
            }
            else
            {
                masterPageItems.Add(new MasterPageItem
                {
                    Title = "Messages",
                    ItemType = ItemMaster.ActionOnCreatedMessages
                });
                masterPageItems.Add(new MasterPageItem
                {
                    Title = "Modifiez vos autorisations",
                    ItemType = ItemMaster.EditAuthorisations
                });
                masterPageItems.Add(new MasterPageItem
                {
                    Title = "Utilisateurs de la salle",
                    ItemType = ItemMaster.UsersRoom
                });
                masterPageItems.Add(new MasterPageItem
                {
                    Title = "Se déconnecter",
                    ItemType = ItemMaster.Logout
                });
            }
            listView.ItemsSource = masterPageItems;
            listView.ItemTemplate = new DataTemplate(() =>
            {
                // Create views with bindings for displaying each property.
                Label titleLabel = new Label();
                titleLabel.SetBinding(Label.TextProperty, "Title");

                return new ViewCell
                {
                    View = new StackLayout
                    {
                        Padding = new Thickness(0, 0),
                        Orientation = StackOrientation.Horizontal,
                        Children =
                                {
                                    titleLabel
                                }
                    }
                };
            });
        }
        #endregion
    }
}
