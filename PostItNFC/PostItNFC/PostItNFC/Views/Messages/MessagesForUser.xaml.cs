﻿using System.Collections.Generic;
using PostItNFC.Model;

using Xamarin.Forms;

namespace PostItNFC.Views.Messages
{
    public partial class MessagesForUser : ContentPage
    {
        #region MessagesForUser fields
        private List<Message> Messages;
        #endregion

        #region MessagesForUser constructor
        public MessagesForUser(List<Message> messages)
        {
            InitializeComponent();
            Title = "Messages";
            Messages = messages;
            CreateListviewTemplate();
            messagesFind.ItemTapped += (s, e) => OnItemTapped(s, e);
        }
        #endregion

        #region MessagesForUser events methods
        private void OnItemTapped(object sender, ItemTappedEventArgs e)
        {
            // don't put in perspective the item selected
            messagesFind.SelectedItem = null;
        }
        #endregion

        #region MessagesForUser set view
        private void CreateListviewTemplate()
        {
            messagesFind.ItemsSource = Messages;
            messagesFind.ItemTemplate = new DataTemplate(() =>
            {
                // Create views with bindings for displaying each property.
                Label firstName = new Label();
                firstName.SetBinding(Label.TextProperty, "AuthorFirstName");
                firstName.TextColor = Color.Teal;

                Label lastName = new Label();
                lastName.SetBinding(Label.TextProperty, "AuthorLastName");
                lastName.TextColor = Color.Teal;
                StackLayout authorLayout = new StackLayout();
                authorLayout.Orientation = StackOrientation.Horizontal;
                authorLayout.Children.Add(new Label { Text = "Author : " });
                authorLayout.Padding = new Thickness(0, 0);
                authorLayout.Margin = new Thickness(0, 0);
                authorLayout.Children.Add(firstName);
                authorLayout.Children.Add(lastName);

                Label date = new Label();
                date.SetBinding(Label.TextProperty, "CreationDate");
                date.TextColor = Color.Teal;
                StackLayout dateLayout = new StackLayout();
                dateLayout.Padding = new Thickness(0, 0);
                dateLayout.Margin = new Thickness(0, 0);
                dateLayout.Orientation = StackOrientation.Horizontal;
                dateLayout.Children.Add(new Label { Text = "Date : " });
                dateLayout.Children.Add(date);

                Label content = new Label();
                content.SetBinding(Label.TextProperty, "Content");
                content.TextColor = Color.Navy;
                StackLayout contentLayout = new StackLayout();
                contentLayout.Padding = new Thickness(0, 0);
                contentLayout.Margin = new Thickness(0, 0);
                contentLayout.Orientation = StackOrientation.Horizontal;
                contentLayout.Children.Add(new Label { Text = "Content : " });
                contentLayout.Children.Add(content);


                return new ViewCell
                {
                    View = new StackLayout
                    {
                        Padding = new Thickness(10, 5),
                        Orientation = StackOrientation.Vertical,
                        Children =
                                {
                                    authorLayout,
                                    dateLayout,
                                    contentLayout
                                }
                    }
                };
            });
        }
        #endregion
    }
}
