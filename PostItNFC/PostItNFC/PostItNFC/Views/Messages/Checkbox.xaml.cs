﻿using System;
using Xamarin.Forms;

namespace PostItNFC.Views.Messages
{
    public partial class Checkbox : ContentView
    {
        #region CheckBbox Fields
        public string CheckboxText { get { return checkboxText.Text; } set { checkboxText.Text = value; } }

        public bool IsActivated { get; set; } = false;

        public EventHandler CheckboxActivated;
        #endregion

        #region Checkbox constructor
        public Checkbox()
        {
            InitializeComponent();
            Padding = Device.OnPlatform(new Thickness(0, 20, 0, 0),
                         new Thickness(0),
                         new Thickness(0));

            TapGestureRecognizer t = new TapGestureRecognizer();
            t.Tapped += OnTapped;
            checkboxLayout.GestureRecognizers.Add(t);

            checkItem.Text = "\u2610";

        }
        #endregion

        #region Checkbox Events
        public void OnTapped(object sender, EventArgs args)
        {
            checkItem.Text = checkItem.Text == "\u2611" ? "\u2610" : "\u2611"; // \u2611 Uni code will show checked Box
            IsActivated = !IsActivated;
            CheckboxActivated?.Invoke(sender,args);
        }
        #endregion

        #region Checkbox methods

        public void Check()
        {
            checkItem.Text = "\u2611";
            IsActivated = true;
        }
        #endregion
    }
}
