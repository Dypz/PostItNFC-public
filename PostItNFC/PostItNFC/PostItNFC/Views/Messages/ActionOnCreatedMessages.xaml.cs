﻿using PostItNFC.Model;
using System.Collections.Generic;
using PostItNFC.Services;

using Xamarin.Forms;
using System;

namespace PostItNFC.Views.Messages
{
    public partial class ActionOnCreatedMessages : ContentPage
    {
        #region ActionOnCreatedMessages Fields
        private List<Message> CreatedMessages { get; set; }

        private User CurrentUser { get; set; }

        private Message SelectedMessage { get; set; } = null;
        #endregion

        #region ActionOnCreatedMessages constructors
        public ActionOnCreatedMessages(User user, List<Message> createdMessages)
        {
            InitializeComponent();

            // Init own class fields
            CurrentUser = user;
            CreatedMessages = createdMessages;

            // Set view
            AnyCreatedMessages();
            SetListViewTemplate();
            IsUserAssociatedToRoom();
        }
        #endregion

        #region ActionOnCreatedMessages events
        private void OnItemTapped(object sender, ItemTappedEventArgs e)
        {
            // we don't want to act on item deselection
            if (e.Item == null || SelectedMessage == (Message)e.Item) return;
            var messageSelected = (Message)e.Item;
            SelectedMessage = messageSelected;
            DisplayChoices();
        }

        private async void DisplayChoices()
        {
            // global ref of app
            var mainPage = GlobalConstants.GetMainPage();

            var answer = await DisplayActionSheet("Select action for the selected message : Content : " + SelectedMessage.Content +
                " created at : " + SelectedMessage.CreationDate + " : ", "Cancel", null, "Remove", "Edit");
            if (answer.ToString().Equals("Remove"))
            {
                if (RESTMessageService.RemoveMessage(SelectedMessage.MessageID))
                {
                    await DisplayAlert("Sucess ! ", "Message succesfully removed", "OK");
                }
                else
                {
                    await DisplayAlert("Alert ! ", "A problem occured when trying to remove the message, try again", "OK");
                }
                
                mainPage.Detail = new NavigationPage(new ManagerUsersRoom.UsersFindForRoom(CurrentUser));
            }
            else if(answer.ToString().Equals("Edit"))
            {
                mainPage.Detail = new NavigationPage(new LetMessage(CurrentUser, SelectedMessage));
            }
            else if (answer.ToString().Equals("Cancel"))
            {
                mainPage.Detail = new NavigationPage(new ActionOnCreatedMessages(CurrentUser,CreatedMessages));
            }
        }

        private void OnLetMessage(object sender, EventArgs e)
        {
            if (CurrentUser == null)
            {
                DisplayAlert("Alert ! ", "A problem occured", "OK");
                return;
            }
            var mainPage = GlobalConstants.GetMainPage();
            mainPage.Detail = new NavigationPage(new LetMessage(CurrentUser));
        }

        #endregion

        #region ActionOnCreatedMessages set views

        private void AnyCreatedMessages()
        {
            if(CreatedMessages == null || CreatedMessages.Count == 0)
            {
                messagesRemovable.IsVisible = false;
                header.Text = "Vous n'avez enregistré aucun message jusqu'à présent";
            }
            else
            {
                messagesRemovable.ItemTapped += (s, e) => OnItemTapped(s, e);
            }
        }
        private void IsUserAssociatedToRoom()
        {
            // a connected user can let a message if associated to any room
            if (CurrentUser.IdRoom != null)
            {
                letMessage.IsVisible = true;
                letMessage.Clicked += (s, e) => OnLetMessage(s, e);
            }
            else
            {
                // otherwise he cannot
                letMessage.IsVisible = false;
                header.IsVisible = false;
                noRoom.IsVisible = true;
            }
        }

        private void SetListViewTemplate()
        {
            messagesRemovable.ItemsSource = CreatedMessages;
            messagesRemovable.ItemTemplate = new DataTemplate(() =>
            {
                // Create views with bindings for displaying each property.


                Label expirationDate = new Label();
                expirationDate.SetBinding(Label.TextProperty, "ExpirationDate");
                expirationDate.TextColor = Color.Black;
                expirationDate.FontSize = 12;
                StackLayout expirationLayout = new StackLayout();
                expirationLayout.Orientation = StackOrientation.Horizontal;
                expirationLayout.Children.Add(new Label { Text = "Expire le",FontSize=12 });
                expirationLayout.Children.Add(expirationDate);
                expirationLayout.HorizontalOptions = LayoutOptions.End;


                Label content = new Label();
                content.SetBinding(Label.TextProperty, "Content");
                content.TextColor = Color.Black;
                StackLayout contentLayout = new StackLayout();
                contentLayout.Orientation = StackOrientation.Vertical;
                contentLayout.Children.Add(content);
                contentLayout.Children.Add(expirationLayout);
                contentLayout.BackgroundColor = Color.FromHex("#FFFFE0");
                contentLayout.Padding = new Thickness(3,3);

                Label creationDate = new Label();
                creationDate.SetBinding(Label.TextProperty, "CreationDate");
                creationDate.TextColor = Color.Black;
                StackLayout creationLayout = new StackLayout();
                creationLayout.Orientation = StackOrientation.Horizontal;
                creationLayout.Children.Add(new Label { Text = "Créé le" });
                creationLayout.Children.Add(creationDate);

                Label security = new Label();
                security.SetBinding(Label.TextProperty, "Security");
                security.TextColor = Color.Teal;
                StackLayout securityLayout = new StackLayout();
                securityLayout.Orientation = StackOrientation.Horizontal;
                securityLayout.Children.Add(new Label { Text = "Destinataire : " });
                securityLayout.Children.Add(security);

                return new ViewCell
                {
                    View = new StackLayout
                    {
                        Padding = new Thickness(10, 10),
                        Orientation = StackOrientation.Vertical,
                        Children =
                                {
                                    securityLayout,
                                    creationLayout,
                                    new Label { Text = "Contenu : " },
                                    contentLayout,
                                }
                    }
                };
            });
        }
        #endregion
    }
}
