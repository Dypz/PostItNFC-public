﻿using PostItNFC.Model;
using PostItNFC.Services;
using PostItNFC.Views.ManagerUsersRoom;
using System;
using Xamarin.Forms;

namespace PostItNFC.Views.Messages
{
    public partial class LetMessage : ContentPage
    {
        #region LetMessage Fields
        private User CurrentUser { get; set; } = null;

        private Message InitMessage { get; set; } = null;
        #endregion

        #region LetMessage Constructor
        public LetMessage(User currentUser)
        {
            InitializeComponent();
            CurrentUser = currentUser;
            sendButton.Clicked += (s, e) => OnSendMessage(s, e);
            sendButton.Text = "Créer le message";
            checkboxPrivate.CheckboxActivated += (s, e) => defineReceiversLayout.IsVisible = !defineReceiversLayout.IsVisible; ;
        }

        public LetMessage(User currentUser, Message initMessage) : this(currentUser)
        {
            InitMessage = initMessage;
            sendButton.Text = "Mettre à jour le message";
            InitCheckboxes();
            mExpirationDate.Date = initMessage.ExpirationDate;
            mExpirationTime.Time = new TimeSpan(initMessage.ExpirationDate.Hour, initMessage.ExpirationDate.Minute, initMessage.ExpirationDate.Second);
            message.Text = initMessage.Content;
            defineReceiverEntry.Text = initMessage.ReceiversList;
        }
        #endregion

        #region LetMessage events

        public void OnSendMessage(object sender, EventArgs e)
        {
            if(!checkboxPrivate.IsActivated && !checkboxPublic.IsActivated)
            {
                DisplayAlert("Alert ! ", "Please select a security level", "OK");
                return;
            }

            if(checkboxPrivate.IsActivated && checkboxPublic.IsActivated)
            {
                DisplayAlert("Alert ! ", "Security level of the message can't be both public and private, select just one", "OK");
                return;
            }

          
            // when the security level is private, the list of receivers can't be empty
            // it must be all or a list of email separated by ';'
            if (checkboxPrivate.IsActivated && String.IsNullOrEmpty(defineReceiverEntry.Text))
            {
                DisplayAlert("Alert ! ", "The list of receivers can't be empty", "OK");
                return;
            }

            DateTime expirationDate = new DateTime(mExpirationDate.Date.Year, mExpirationDate.Date.Month, mExpirationDate.Date.Day,
                                                   mExpirationTime.Time.Hours, mExpirationTime.Time.Minutes, mExpirationTime.Time.Seconds);

            // if InitMessage is null, the user is currently creating a message
            if (InitMessage == null)
            {               
                // create the new message and add it to the database
                if (RESTMessageService.CreateMessage(CreateMessage(expirationDate)))
                { DisplayAlert("Success ! ", "Message succesfully created", "OK"); }
                else
                { DisplayAlert("Alert ! ", "A problem occured when creating the message, try again", "OK"); }
            }
            else
            {
                // update appropriate fields of the message included the creation date
                UpdateMessage(expirationDate);

                // update message in database
                if (RESTMessageService.UpdateMessage(InitMessage))
                { DisplayAlert("Success ! ", "Message succesfully updated", "OK"); }
                else
                { DisplayAlert("Alert ! ", "A problem occured when updating the message, try again", "OK"); }
            }
            var mainPage = GlobalConstants.GetMainPage();
            mainPage.Detail= new NavigationPage(new UsersFindForRoom(CurrentUser));
        }

        #endregion

        #region LetMessage methods
        private void InitCheckboxes()
        {
            if (InitMessage.Security == SecurityLevel.AuthenticatedUsers || InitMessage.Security == SecurityLevel.ReceiversDefine)
            {
                checkboxPrivate.Check();
                defineReceiversLayout.IsVisible = true;
            }
            else
            { checkboxPublic.Check(); }
        }

        private SecurityLevel DefineSecurity()
        {
            if (checkboxPrivate.IsActivated)
            {
                if (defineReceiverEntry.Text.Equals("all"))
                    return SecurityLevel.AuthenticatedUsers;
                return SecurityLevel.ReceiversDefine;
            }
            else
            {
                return SecurityLevel.Public;
            }
        }

        private Message CreateMessage(DateTime expirationDate)
        {
            Message createdMessage = new Message(message.Text, CurrentUser.Id, CurrentUser.FirstName, CurrentUser.LastName, DateTime.Now, expirationDate, defineReceiverEntry.Text, CurrentUser.IdRoom);
            createdMessage.Security = DefineSecurity();
            return createdMessage;
        }

        private void UpdateMessage(DateTime expirationDate)
        {
            InitMessage.ExpirationDate = expirationDate;
            InitMessage.Content = message.Text;
            InitMessage.Security = DefineSecurity();
            InitMessage.ReceiversList = defineReceiverEntry.Text;
            InitMessage.CreationDate = DateTime.Now;
        }

        #endregion
    }
}
