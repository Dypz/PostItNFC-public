﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Graph;
using Microsoft.Identity.Client;
using PostItNFC.Helper;
using Xamarin.Forms;

namespace PostItNFC.Views
{
    public partial class UserSelector : ContentView
    {
        #region UserSelector Attributes
        public event EventHandler UsersReceived;
        public event EventHandler Login;

        public IPlatformParameters platformParameters { get; set; }
        private static GraphServiceClient graphClient = null;
        #endregion



        #region UserSelector constructor & Init events
        public UserSelector()
        {
            InitializeComponent();
            guest.Clicked += (s, e) => onUsersReceived(s, e);
            registered.Clicked += (s, e) => onLogin(s, e);
        }
        #endregion

        #region Methods events
        private void onUsersReceived(object sender, EventArgs e)
        {
            UsersReceived?.Invoke(this, e);
        }

        private void onLogin(object sender, EventArgs e)
        {
            Login?.Invoke(this, e);
        }

        private async void signIn_OnClicked(object sender, EventArgs e)
        {

            graphClient = AuthenticationHelper.GetAuthenticatedClient();
            var currentUserObject = await graphClient.Me.Request().GetAsync();
            //var currentUserContact = await graphClient.DirectoryObjects.Request().GetAsync();
            App.Username = currentUserObject.DisplayName;
            App.UserEmail = currentUserObject.UserPrincipalName;
            App.UserID = currentUserObject.Id;
            System.Diagnostics.Debug.WriteLine(App.Username);

            // await DisplayAlert("Welcome", userName, "Ok", "Cancel");
            // await Navigation.PushAsync(new HomePage());
        }

        #endregion
    }
}