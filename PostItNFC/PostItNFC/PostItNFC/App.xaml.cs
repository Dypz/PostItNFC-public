﻿using Microsoft.IdentityModel.Clients.ActiveDirectory;
using System;
using System.Net.Http;
using PostItNFC.Model;
using PostItNFC.Services;
using Xamarin.Forms;
using PostItNFC.Helper;
using System.Threading.Tasks;
using NdefLibrary.Ndef;
using Poz1.NFCForms.Abstract;

namespace PostItNFC
{
    public partial class App : Application
    {
        #region App fields
        public static string ClientId = "b1d2ab07-5908-4e4a-abc5-879087d5db4c";
        public static string LoginAuthority = "https://login.microsoftonline.com/arnaudannuaire.onmicrosoft.com";
        public static string ReturnUri = "https://graph.windows.net/039412ab-08fa-4021-a4cb-b6c808142f18";
        public static string GraphResourceUri = "https://graph.microsoft.com";
        public static AuthenticationResult AuthenticationResult = null;

        private readonly INfcForms device;
        private string roomId = null;
        public static User UserObtained;
        #endregion

        #region App constructor
        public App()
        {
            RetrieveDirectory();
            // we can tag a room only if we are on a mobile device
            if(Device.OS != TargetPlatform.Windows)
            {
                device = DependencyService.Get<INfcForms>();
                device.NewTag += handleNewTag;
            }
            if(roomId == null)
            {
                Func<Task> task = async () => {
                    UserObtained = await AuthenticationHelper.SignIn(false);
                };
                task().Wait();
                // we force the app to wait the end of the task to get the token cache
                MainPage = new MainPage("C601", UserObtained);
            }
            
        }
        #endregion

        #region App methods

        protected override void OnStart()
        {
            //UserObtained = await AuthenticationHelper.SignIn(false);
        }

        protected override void OnSleep()
        {
            // Handle when your app sleeps
        }

        protected override void OnResume()
        {
            // Handle when your app resumes
        }
        private void RetrieveDirectory()
        {
            try
            {
                using (var client = new HttpClient())
                {
                    var url = GlobalConstants.DirectoryRetrieverURI+"User/";
                    HttpResponseMessage wcfResponse = client.GetAsync(url).Result;
                    HttpContent stream = wcfResponse.Content;
                    var data = stream.ReadAsStringAsync();
                    MicrosoftAPIs.SetAllUsers(data.Result);
                }
            }
            catch (Exception ex)
            {

                System.Diagnostics.Debug.WriteLine("EXCEPTION " + ex.Message);
            }
        }

        private string readNdefMessage(NdefMessage message)
        {
            string result = "";
            foreach (NdefRecord record in message)
            {
                if(record.CheckSpecializedType(false) == typeof(NdefTextRecord))
                {
                    var tRecord = new NdefTextRecord(record);
                    result = tRecord.Text;
                }
            }
            return result;
        }

        private void handleNewTag(object sender, NfcFormsTag e)
        {
            if(e != null && e.IsNdefSupported)
            {
                roomId = readNdefMessage(e.NdefMessage);
                Func<Task> task = async () => {
                    UserObtained = await AuthenticationHelper.SignIn(false);
                };
                task().Wait();
                MainPage = new MainPage(roomId, UserObtained);
            }
        }

        #endregion

    }
}
