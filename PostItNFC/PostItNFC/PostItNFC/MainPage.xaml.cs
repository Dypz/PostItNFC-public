﻿using Xamarin.Forms;
using PostItNFC.Model;
using System;
using PostItNFC.Helper;
using PostItNFC.Views.ManagerUsersRoom;
using PostItNFC.Views.Authorisations;
using PostItNFC.Services;
using PostItNFC.Views.MasterDetails;

namespace PostItNFC
{
    public partial class MainPage : MasterDetailPage
    {
        #region MainPage Fields

        public MasterPage mPage;
        public User CurrentUser;
        private MasterPageItem SelectedItem = null;

        #endregion

        #region MainPage constructor

        public MainPage(string roomTagged,User user)
        {
            InitializeComponent();
            GlobalConstants.RoomTagged = roomTagged;
            InitMaster(user);
        }
        #endregion

        #region Methods events

        private void InitMaster(User user)
        {
            if (user != null)
            {
                CurrentUser = user;
                mPage = new MasterPage(UsersRole.Role.Student, user.FirstName + " " + user.LastName);
                Detail = new NavigationPage(new UsersFindForRoom(user));
            }
            else
            {
                mPage = new MasterPage(UsersRole.Role.Guest,"Guest");
                Detail = new NavigationPage(new UsersFindForRoom(UsersRole.Role.Guest));
            }
            mPage.ListView.ItemTapped += OnItemTapped;
            Master = mPage;
        }

        private void OnItemTapped(object sender, ItemTappedEventArgs e)
        {
            var item = e.Item as MasterPageItem;
            if (item == null) return;
            SelectedItem = item;
            switch (item.ItemType)
            {
                case ItemMaster.DisplayName:
                    // do nothing if the user click on his name
                    break;
                case ItemMaster.Login:
                    SignIn();
                    break;
                case ItemMaster.ActionOnCreatedMessages:
                    Detail = new NavigationPage(new Views.Messages.ActionOnCreatedMessages(CurrentUser, RESTMessageService.GetCreatedMessages(CurrentUser.Id)));
                    break;
                case ItemMaster.EditAuthorisations:
                    Detail = new NavigationPage(new SetAuthorisationsOnDatas(CurrentUser));
                    break;
                case ItemMaster.UsersRoom:
                    Detail = new NavigationPage(new UsersFindForRoom(CurrentUser));
                    break;
                case ItemMaster.Logout:
                    AuthenticationHelper.SignOut();
                    InitMaster(null);
                    break;
            }
            mPage.ListView.SelectedItem = null;
            IsPresented = false;
        }

        private async void SignIn()
        {
            User user = await AuthenticationHelper.SignIn(true);
            InitMaster(user);
        }
        #endregion

        
    }
}
