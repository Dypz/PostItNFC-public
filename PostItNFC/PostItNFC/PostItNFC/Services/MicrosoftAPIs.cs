﻿using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using Newtonsoft.Json;
using PostItNFC.Model;

namespace PostItNFC.Services
{
    static public class MicrosoftAPIs
    {
        public static List<User> users = new List<User>();

        static public string GetRoomOfUser(string userId)
        {

            foreach (User user in users)
            {
                if (user.Mail == userId)
                {
                    return user.IdRoom;
                }
            }
            return null;

        }
        
        static public void SetAllUsers(string jsonString)
        {
            try
            {

                dynamic res = JsonConvert.DeserializeObject<Dictionary<string, dynamic>>(jsonString);
                List<User> userFind = new List<User>();

                foreach (string value in res.Values)
                {
                    dynamic resultat = JsonConvert.DeserializeObject<Dictionary<string, dynamic>>(value);
                    foreach (string key in resultat.Keys)
                    {
                        if (key.Equals("value"))
                        {
                            JArray userInformationsArray = (JArray) resultat["value"];

                            for (var i = 0; i < userInformationsArray.Count; i++)
                            {

                                    var firstName = (string) userInformationsArray[i]["givenName"];
                                    var lastName = (string) userInformationsArray[i]["surname"];

                                    var id = (string) userInformationsArray[i]["id"];
                                    var roomId = (string) userInformationsArray[i]["officeLocation"];

                                    var email = (string) userInformationsArray[i]["userPrincipalName"];
                                    userFind.Add(new User(firstName, lastName, email, id, roomId,
                                        UsersRole.Role.Student));

                                }
                            }
                        }
                    }
                 users = userFind;
            }



            catch (JsonException je)
            {
                System.Diagnostics.Debug.WriteLine("Error in deserialisation : should verify format of your json input");
                System.Diagnostics.Debug.WriteLine(je.Message);
                return ;
            }
            catch (Exception e)
            {
                System.Diagnostics.Debug.WriteLine("Unexpected error during deserialisation");
                System.Diagnostics.Debug.WriteLine(e.Message);
                return;
            }

        }


        static public List<User> GetUserOfRoom(string roomExpected)
        {
            List<User> roomsUsers = new List<User>();
            foreach (User user in users)
            {
                if (user.IdRoom == roomExpected)
                {
                     roomsUsers.Add(user);
                }
            }
            return roomsUsers;
        }
    }

}
