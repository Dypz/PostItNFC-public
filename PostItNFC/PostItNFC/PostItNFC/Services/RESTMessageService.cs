﻿using Newtonsoft.Json;
using PostItNFC.Model;
using System;
using System.Collections.Generic;
using System.IO;
using System.Net.Http;
using System.Runtime.Serialization.Json;
using System.Text;

namespace PostItNFC.Services
{
    static public class RESTMessageService
    {
        static public List<Message> GetAvailableMessagesForRoom(string userID, string roomID)
        {
            try
            {
                HttpClient client = new HttpClient();
                HttpResponseMessage wcfResponse = client.GetAsync(GlobalConstants.MessageURI + "GetMessagesRoom/id="+userID+"/roomID="+roomID).Result;
                HttpContent stream = wcfResponse.Content;
                var data = stream.ReadAsStringAsync();
                AvailableMessageCollection messages = JsonConvert.DeserializeObject<AvailableMessageCollection>(data.Result);
                return messages.Messages;
            }
            catch (NullReferenceException ne)
            {
                return null;
            }
            catch (Exception ex)
            {
                return null;
            }
            
        }

        static public List<Message> GetCreatedMessages(string userID)
        {
            try
            {
                HttpClient client = new HttpClient();
                HttpResponseMessage wcfResponse = client.GetAsync(GlobalConstants.MessageURI + "GetCreatedMessages/id="+userID).Result;
                HttpContent stream = wcfResponse.Content;
                var data = stream.ReadAsStringAsync();
                CreatedMessageCollection messages = JsonConvert.DeserializeObject<CreatedMessageCollection>(data.Result);
                return messages.Messages;
            }
            catch (NullReferenceException)
            {
                return null;
            }
            catch (Exception)
            {
                return null;
            }

        }

        static private StringContent GetRequest(Message message)
        {
            DataContractJsonSerializer serData = new DataContractJsonSerializer(typeof(Message));
            MemoryStream mem = new MemoryStream();
            serData.WriteObject(mem, message);
            string messageSerialized = Encoding.UTF8.GetString(mem.ToArray(), 0, (int)mem.Length);
            var request = new StringContent(messageSerialized, Encoding.UTF8, "application/json");
            return request;
        }

        static public bool CreateMessage(Message message)
        {
            using (HttpClient client = new HttpClient())
            {
                StringContent request = GetRequest(message);

                Uri uri = new Uri(GlobalConstants.MessageURI + "CreateMessage/");
                var response = client.PostAsync(uri, request).Result;

                return response.IsSuccessStatusCode;
            }
        }

        static public bool UpdateMessage(Message message)
        {
            using (HttpClient client = new HttpClient())
            {
                StringContent request = GetRequest(message);

                Uri uri = new Uri(GlobalConstants.MessageURI + "UpdateMessage/");
                var response = client.PostAsync(uri, request).Result;

                return response.IsSuccessStatusCode;
            }
        }

        static public bool RemoveMessage(string messageID)
        {
            using (HttpClient client = new HttpClient())
            {
                System.Diagnostics.Debug.WriteLine(messageID);
                Uri uri = new Uri(GlobalConstants.MessageURI + "RemoveMessage/id=" + messageID);
                var response = client.GetAsync(uri).Result;

                return response.IsSuccessStatusCode;
            }
        }


    }
}
