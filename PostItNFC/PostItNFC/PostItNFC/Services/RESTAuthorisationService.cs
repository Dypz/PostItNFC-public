﻿using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using PostItNFC.Model;
using System;
using System.Collections.Generic;
using System.IO;
using System.Net.Http;
using System.Runtime.Serialization.Json;
using System.Text;

namespace PostItNFC.Services
{
    static public class RESTAuthorisationService
    {
        static public Authorisation GetAuthorisation(string userID)
        {
            try
            {
                HttpClient client = new HttpClient();
                HttpResponseMessage wcfResponse = client.GetAsync(GlobalConstants.AuthorisationURI + "GetAuthorisations?id=" + userID).Result;
                HttpContent httpContent = wcfResponse.Content;
                var data = httpContent.ReadAsStringAsync();
                // should throw a null reference exception if no authorisations found ( null )
                JObject jsonObj = JObject.Parse(data.Result);
                Dictionary<string, object> dictObj = jsonObj.ToObject<Dictionary<string, object>>();
                Authorisation authorisation = JsonConvert.DeserializeObject<Authorisation>(dictObj["GetUserAuthorisationsResult"].ToString());
                return authorisation;
            }
            // if we catch a null reference exception, no authorisations are found so we return null
            catch(NullReferenceException)
            {
                return null;
            }
            catch (Exception)
            {
                return null;
            }
        }

        static public Authorisation GetGuestAuthorisation(string mail)
        {
            try
            {
                HttpClient client = new HttpClient();
                HttpResponseMessage wcfResponse = client.GetAsync(GlobalConstants.AuthorisationURI + "GetGuestAuthorisations?mail=" + mail).Result;
                HttpContent httpContent = wcfResponse.Content;
                var data = httpContent.ReadAsStringAsync();
                // should throw a null reference exception if no authorisations found ( null )
                JObject jsonObj = JObject.Parse(data.Result);
                Dictionary<string, object> dictObj = jsonObj.ToObject<Dictionary<string, object>>();
                Authorisation authorisation = JsonConvert.DeserializeObject<Authorisation>(dictObj["GetGuestAuthorisationsResult"].ToString());
                return authorisation;
            }
            // if we catch a null reference exception, no authorisations are found so we return null
            catch (NullReferenceException)
            {
                return null;
            }
            catch (Exception)
            {
                return null;
            }
        }

        static private StringContent GetRequest(Authorisation authorisation)
        {
            DataContractJsonSerializer serData = new DataContractJsonSerializer(typeof(Authorisation));
            MemoryStream mem = new MemoryStream();
            serData.WriteObject(mem, authorisation);
            string authorisationSerialized = Encoding.UTF8.GetString(mem.ToArray(), 0, (int)mem.Length);
            var request = new StringContent(authorisationSerialized, Encoding.UTF8, "application/json");
            return request;
        }

        static public bool SetAuthorisations(Authorisation authorisation)
        {
            using (HttpClient client = new HttpClient())
            {
                StringContent request = GetRequest(authorisation);

                Uri uri = new Uri(GlobalConstants.AuthorisationURI + "SetAuthorisations/");
                var response = client.PostAsync(uri, request).Result;

                return response.IsSuccessStatusCode;
            }
        }

        static public bool UpdateAuthorisation(Authorisation authorisation)
        {
            using (HttpClient client = new HttpClient())
            {
                StringContent request = GetRequest(authorisation);
                Uri uri = new Uri(GlobalConstants.AuthorisationURI + "UpdateAuthorisations/");
                var response = client.PostAsync(uri, request).Result;

                return response.IsSuccessStatusCode;
            }
        }
    }
}
