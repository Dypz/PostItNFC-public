﻿using Xamarin.Forms;


namespace PostItNFC.Model
{
    static public class GlobalConstants
    {
        public static string RoomTagged = null;

        public static string MessageURI = "http://databaserestservice.cloudapp.net/Messages/MessageService.svc/";
        public static string AuthorisationURI = "http://databaserestservice.cloudapp.net/Authorisations/AuthorisationService.svc/";
        public static string DirectoryRetrieverURI = "http://postitnfc.cloudapp.net/DirectoryRetrieverService.svc/";

        public static MainPage GetMainPage()
        {
            var app = Application.Current as App;
            var mainPage = (MainPage)app.MainPage;
            return mainPage;
        }
    }
}
