﻿namespace PostItNFC.Model
{
    public class MasterPageItem
    {
        public string Title { get; set; }

       public ItemMaster ItemType { get; set; }
    }

    public enum ItemMaster { DisplayName, Login, ActionOnCreatedMessages, EditAuthorisations, UsersRoom, Logout }
}
