﻿
namespace PostItNFC.Model
{
    public class FieldsUser
    {
        #region FieldsUser Fields
        public string FieldName { get; set; }

        public string DatabaseName { get; set; }

        public bool Enabled { get; set; }

        #endregion
        
        #region FieldsUser constructor
        public FieldsUser(string fieldName, bool enabled)
        {
            FieldName = fieldName;
            Enabled = enabled;
            setDataBaseName();
        }

        #endregion

        #region FieldsUser methods
        private void setDataBaseName()
        {
            DatabaseName = FieldName.Replace(" ", string.Empty);
        }
        #endregion
    }
}
