﻿using Newtonsoft.Json;
using System.Collections.Generic;
using System.Runtime.Serialization;

namespace PostItNFC.Model
{
    [DataContract]
    public class Authorisation
    {
        #region Authorisation Fields
        [DataMember]
        [JsonProperty("FirstNameAut")]
        public bool FirstNameAut { get; set; }

        [DataMember]
        [JsonProperty("LastNameAut")]
        public bool LastNameAut { get; set; }

        [DataMember]
        [JsonProperty("MailAut")]
        public bool MailAut { get; set; }

        [DataMember]
        [JsonProperty("RoomAut")]
        public bool RoomAut { get; set; }

        [DataMember]
        [JsonProperty("RoleAut")]
        public bool RoleAut { get; set; }

        [DataMember]
        [JsonProperty("UserID")]
        public string UserID { get; set; } = null;

        [DataMember]
        [JsonProperty("UserMail")]
        public string UserMail { get; set; } = null;

        #endregion

        #region Authorisation constructors

        public Authorisation() { }
        public Authorisation(bool firstNameAut, bool lastNameAut, bool mailAut, bool roomAut, bool roleAut, string userID, string userMail)
        {
            FirstNameAut = firstNameAut;
            LastNameAut = lastNameAut;
            MailAut = mailAut;
            RoomAut = roomAut;
            RoleAut = roleAut;
            UserID = userID;
            UserMail = userMail;
        }

        #endregion

        #region Authorisations methods

        public override string ToString()
        {
            return "FirstName : " + FirstNameAut + " LastName : " + LastNameAut + " Mail : " + MailAut + " UserID : " + UserID;
        }

        public Dictionary<string, bool> ExploreAuth()
        {
            Dictionary<string, bool> authorisations = new Dictionary<string, bool>();
            authorisations.Add("First Name", this.FirstNameAut);
            authorisations.Add("Last Name", this.LastNameAut);
            authorisations.Add("Mail", this.MailAut);
            authorisations.Add("Role", this.RoleAut);
            authorisations.Add("Id Room", this.RoomAut);
            return authorisations;
        }
        #endregion
    }
}
