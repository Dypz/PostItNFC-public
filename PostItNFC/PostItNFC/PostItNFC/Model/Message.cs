﻿using Newtonsoft.Json;
using Newtonsoft.Json.Converters;
using System;
using System.Collections.Generic;
using System.Runtime.Serialization;

namespace PostItNFC.Model
{
    [DataContract]
    public class Message
    {
        #region Message Fields
        [DataMember]
        [JsonProperty("Content")]
        public string Content { get; set; }

        [DataMember]
        [JsonProperty("Security")]
        [JsonConverter(typeof(StringEnumConverter))]
        public SecurityLevel Security { get; set; }

        [DataMember]
        [JsonProperty("AuthorID")]
        public string AuthorID { get; set; }

        [DataMember]
        [JsonProperty("MessageID")]
        public string MessageID { get; set; }

        [DataMember]
        [JsonProperty("AuthorFirstName")]
        public string AuthorFirstName { get; set; }

        [DataMember]
        [JsonProperty("AuthorLastName")]
        public string AuthorLastName { get; set; }

        [DataMember]
        [JsonProperty("CreationDate")]
        public DateTime CreationDate { get; set; }

        [DataMember]
        [JsonProperty("ExpirationDate")]
        public DateTime ExpirationDate { get; set; }

        [DataMember]
        [JsonProperty("ReceiversList")]
        public string ReceiversList { get; set; } = null;

        [DataMember]
        [JsonProperty("AssociatedRoomID")]
        public string AssociatedRoomID { get; set; }

        #endregion

        #region Message constructor
        // default constructor primordial for deserialization !
        public Message() { }

        public Message(string content, string authorID, string authorFirstName, string authorLastName, DateTime creationDate, DateTime expirationDate, string receiversList, string associatedRoomID)
        {
            Content = content;
            AuthorID = authorID;
            AuthorFirstName = authorFirstName;
            AuthorLastName = authorLastName;
            CreationDate = creationDate;
            ExpirationDate = expirationDate;
            ReceiversList = receiversList;
            AssociatedRoomID = associatedRoomID;
            MessageID = GenerateID();
        }

        public Message(string content, SecurityLevel security, string authorID, string authorFirstName, string authorLastName,
            DateTime creationDate, DateTime expirationDate, string receiversList, string associatedRoomID) 
            : this(content, authorID, authorFirstName, authorLastName, creationDate, expirationDate, receiversList, associatedRoomID)
        {
            Security = Security;
        }

        #endregion

        #region Message Methods

        private string GenerateID()
        {
            return Guid.NewGuid().ToString("N");
        }

        #endregion
    }

    public enum SecurityLevel { Public = 0, AuthenticatedUsers = 1, ReceiversDefine = 2}

    public class AvailableMessageCollection
    {
        [JsonProperty("GetMessagesRoomListResult")]
        public List<Message> Messages { get; set; }
    }

    public class CreatedMessageCollection
    {
        [JsonProperty("GetCreatedMessagesResult")]
        public List<Message> Messages { get; set; }
    }


}
