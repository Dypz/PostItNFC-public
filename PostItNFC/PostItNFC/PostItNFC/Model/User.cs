﻿
namespace PostItNFC.Model
{
    public class User
    {
        #region User Fields
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Mail { get; set; }
        public string Id { get; set; } = null;
        public string IdRoom { get; set; } = null;
        public UsersRole.Role Role { get; set; }
        #endregion

        #region User constructors
        public User(){  }

        public User(string firstName, string lastName, string mail, string id, string idRoom)
        {
            FirstName = firstName;
            LastName = lastName;
            Mail = mail;
            Id = id;
            IdRoom = idRoom;
        }

        public User(string firstName, string lastName, string mail,string id,string idRoom, UsersRole.Role role) : 
            this(firstName, lastName, mail, id, idRoom)
        {
            Role = role;
        }
        #endregion

        #region User methods

        public override string ToString()
        {
            return "FirstName : " + FirstName + " LastName : " + LastName + " Mail : " + Mail + " Id : " + Id + " IdRoom : " + IdRoom;
        }

        #endregion

    }
    
     
}
