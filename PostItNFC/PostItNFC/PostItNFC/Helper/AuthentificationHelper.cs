﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading.Tasks;
using Xamarin.Forms;
using PostItNFC.Services;
using PostItNFC.Model;

namespace PostItNFC.Helper
{
    public class AuthenticationHelper
    {
        public static string TokenForUser = null;
        public static DateTimeOffset expiration;

        /// <summary>
        /// Get Token for User.
        /// </summary>
        /// <returns>Token for user.</returns>

        public static async Task<Model.User> SignIn(bool onSignIn)

        {
            try
            {
                var data = await DependencyService.Get<IAuthenticator>()
              .Authenticate(App.LoginAuthority, App.GraphResourceUri, App.ClientId, App.ReturnUri, onSignIn);
                if (data != null)
                {
                    App.AuthenticationResult = data;

                    string firstName = data.UserInfo.GivenName;
                    string lastName = data.UserInfo.FamilyName;
                    string userID = data.UserInfo.UniqueId;
                    string mailYolo = data.UserInfo.DisplayableId;
                    var client = new HttpClient();
                    client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                    client.DefaultRequestHeaders.Authorization =
                        new System.Net.Http.Headers.AuthenticationHeaderValue("Bearer",
                            App.AuthenticationResult.AccessToken);

                    Model.User user = new Model.User(firstName, lastName, mailYolo, userID,
                        MicrosoftAPIs.GetRoomOfUser(mailYolo), UsersRole.Role.Student);
                    App.UserObtained = user;
                    return user;
                }
                return null;

            }
            catch (Exception exception)
            {
                // Do nothing if exception catch, just stay on this page
                System.Diagnostics.Debug.WriteLine(exception.Message);
                return null;
            }
        }


        /// <summary>
        /// Signs the user out of the service.
        /// </summary>
        public static void SignOut()
        {
            DependencyService.Get<IAuthenticator>().LogOut();
        }
    }
}