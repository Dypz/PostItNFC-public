using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using Microsoft.IdentityModel.Clients.ActiveDirectory;
using Xamarin.Forms;
using PostItNFC.Helper;
[assembly: Dependency(typeof(PostItNFC.Droid.Helper.Authenticator))]
namespace PostItNFC.Droid.Helper
{
    class Authenticator : IAuthenticator
    {
        private AuthenticationContext AuthContext;
        public async Task<AuthenticationResult> Authenticate(string authority, string resource, string clientId, string returnUri,bool onClick)
        {
            AuthContext = new AuthenticationContext(authority);
            if (AuthContext.TokenCache.ReadItems().Any())
                AuthContext = new AuthenticationContext(AuthContext.TokenCache.ReadItems().First().Authority);
            else if (!onClick)
                return null;
            var authResult = await AuthContext.AcquireTokenAsync(resource, clientId, new Uri(returnUri), new PlatformParameters((Activity)Forms.Context));
            return authResult;
        }

        public void LogOut()
        {
            AuthContext.TokenCache.Clear();
        }

    }
}