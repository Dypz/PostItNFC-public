﻿using System;

using Android.App;
using Android.Content;
using Android.Content.PM;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using Android.OS;
using Microsoft.IdentityModel.Clients.ActiveDirectory;
using Android.Nfc;
using Poz1.NFCForms.Droid;
using Poz1.NFCForms.Abstract;

namespace PostItNFC.Droid
{
    [Activity(Label = "PostItNFC", Icon = "@drawable/icon", Theme = "@style/MainTheme", MainLauncher = true, ConfigurationChanges = ConfigChanges.ScreenSize | ConfigChanges.Orientation, LaunchMode = LaunchMode.SingleTop)]
    [IntentFilter(new[] { NfcAdapter.ActionNdefDiscovered },
        Categories = new[] { Intent.CategoryDefault },
        DataMimeType = "text/plain")]
    public class MainActivity : global::Xamarin.Forms.Platform.Android.FormsAppCompatActivity
    {
        #region MainActivity.Droid fields
        //For foreground Nfc Handling
        private NfcAdapter NFCDevice;
        private NfcForms x;
        #endregion

        #region MainActivity.Droid events

        protected override void OnActivityResult(int requestCode, Result resultCode, Intent data)
        {
            base.OnActivityResult(requestCode, resultCode, data);
            AuthenticationAgentContinuationHelper.SetAuthenticationAgentContinuationEventArgs(requestCode, resultCode,
                data);
        }
        protected override void OnCreate(Bundle bundle)
        {
            TabLayoutResource = Resource.Layout.Tabbar;
            ToolbarResource = Resource.Layout.Toolbar;

            base.OnCreate(bundle);

            global::Xamarin.Forms.Forms.Init(this, bundle);

            NfcManager nfcManager = (NfcManager)Android.App.Application.Context.GetSystemService(Context.NfcService);
            NFCDevice = nfcManager.DefaultAdapter;

            Xamarin.Forms.DependencyService.Register<INfcForms, NfcForms>();
            x = Xamarin.Forms.DependencyService.Get<INfcForms>() as NfcForms;

            LoadApplication(new App());

            handleIntent(this.Intent);
        }

        protected override void OnResume()
        {
            base.OnResume();
            if (NFCDevice != null)
            {
                var intent = new Intent(this, this.Class).AddFlags(ActivityFlags.SingleTop);
                NFCDevice.EnableForegroundDispatch
                (
                    this,
                    PendingIntent.GetActivity(this, 0, intent, 0),
                    null,
                    null
                );
            }
        }

        protected override void OnPause()
        {
            if(NFCDevice != null)
                NFCDevice.DisableForegroundDispatch(this);
            base.OnPause();
        }

        protected override void OnNewIntent(Intent intent)
        {
            base.OnNewIntent(intent);
            handleIntent(intent);
        }

        protected void handleIntent(Intent intent)
        {
            string action = intent.Action;
            if (NfcAdapter.ActionNdefDiscovered.Equals(action))
            {
                x.OnNewIntent(this, intent);
            }
        }
        #endregion
    }
}

